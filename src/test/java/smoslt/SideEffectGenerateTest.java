package smoslt;

import static org.junit.Assert.*;

import org.junit.Test;

public class SideEffectGenerateTest {

	@Test
	public void testSideEffectRegex() {
		assertTrue("AbcBakLmc".matches(SideEffectGenerate.SIDE_EFFECT_NAME_REGEX));
		assertTrue(!"Abca$".matches(SideEffectGenerate.SIDE_EFFECT_NAME_REGEX));
	}

	@Test
	public void testThreeLetterRegex() {
		assertTrue("Abc".matches(SideEffectGenerate.THREE_LETTER_SORT_NAME_REGEX));
		assertTrue(!"Abca".matches(SideEffectGenerate.THREE_LETTER_SORT_NAME_REGEX));
	}
	
	@Test
	public void testCategorySort() {
		assertTrue("0".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue("3".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue("33".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue("".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue(!"a".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue(!"%".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
		assertTrue(!"105".matches(SideEffectGenerate.CATEGORY_SORT_REGEX));
	}

}
