package smoslt;

import java.util.ArrayList;
import java.util.List;

import org.btrg.uti.FileAsList_;
import org.btrg.uti.NioFileUtil;

public class TemplateString {
	
	public static String getImport(){
		return "This file is part of smoslt.given module, one of many modules that belongs to smoslt\n"
				+"\n"
				+" smoslt.given is free software: you can redistribute it and/or modify\n"
				+" it under the terms of the GNU General Public License as published by\n"
				+" the Free Software Foundation, either version 3 of the License, or\n"
				+" (at your option) any later version.\n"
				+"\n"
				+" smoslt.given is distributed in the hope that it will be useful,\n"
				+" but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
				+" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
				+" GNU General Public License for more details.\n"
				+"\n"
				+" You should have received a copy of the GNU General Public License\n"
				+" along with smoslt.given in the file smoslt.given/GNUGeneralPublicLicensev3.0.html  \n"
				+" If not, see <http://www.gnu.org/licenses/>.\n"
				+" */\n"
				+"package  smoslt.given.sideeffect.out;\n"
				+"\n"
				+"import smoslt.domain.ScoreTracker;\n"
				+"import smoslt.domain.SideEffectBase;\n"
				+"import smoslt.domain.Task;\n"
				+"\n"
				+"public class ";
	}
	
	public static String  getHead(){
		return " extends SideEffectBase {\n"
				+"\n"
				+"    public void go(ScoreTracker scoreTracker, Task task) {\n";
	}
	
	public  static String getTail(){
		return "\n"
				+"		\n"
				+"	}\n"
				+"\n"
				+"\n"
				+"}\n";
	}
	
	List<String> importList;
	List<String> headList;
	List<String> tailList;
	List<String> importOutputList = new ArrayList<String>();
	List<String> headOutputList = new ArrayList<String>();
	List<String> tailOutputList = new ArrayList<String>();

	public static void main(String[] args) {
		new TemplateString().go();
	}

	void go() {
		importList = FileAsList_
				.read("src/main/resources/template/sideEffectImport.txt");
		headList = FileAsList_
				.read("src/main/resources/template/sideEffectHead.txt");
		tailList = FileAsList_
				.read("src/main/resources/template/sideEffectTail.txt");
		for(String line:importList){
			importOutputList.add("+\""+ line + "\\n\"");
		}
		for(String line:headList){
			headOutputList.add("+\""+ line + "\\n\"");
		}
		for(String line:tailList){
			tailOutputList.add("+\""+ line + "\\n\"");
		}
		for(String line:importOutputList){
			NioFileUtil.appendLine("src/main/resources/template/sideEffectImport_.txt", line.getBytes());
		}
		for(String line:headOutputList){
			NioFileUtil.appendLine("src/main/resources/template/sideEffectHead_.txt", line.getBytes());
		}
		for(String line:tailOutputList){
			NioFileUtil.appendLine("src/main/resources/template/sideEffectTail_.txt", line.getBytes());
		}
	}

}
