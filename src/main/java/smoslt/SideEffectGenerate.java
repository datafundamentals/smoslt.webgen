package smoslt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.btrg.uti.StringUtils_;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerImpl;

public class SideEffectGenerate implements ValueChangeListener,
		ListSelectionListener {
	private static final String WORK_COLUMN_NAME = "Work Role Triggers";
	private static final String SETUP_TASK_COLUMN_NAME = "On Setup, for First Task";
	private static final String EACH_TASK_COLUMN_NAME = "At Every Task";
	private static final String SETUP_TASK_CODE_COLUMN_NAME = "Setup Code";
	private static final String EACH_TASK_CODE_COLUMN_NAME = "Task Code";
	final static String NAME_COMPLIANT_REGEX = "^[A-Z][a-z][a-z][0-9]?_[A-Z][a-z].*";
	final static String OR_GROUP_REGEX = "^[A-Z][a-z][a-z][0-9]_[A-Z][a-z].*";
	final static String OR_GROUP_TITLE_REGEX = "^[A-Z][a-z][a-z]0_[A-Z][a-z].*";
	final static String OR_GROUP_MEMBER_REGEX = "^[A-Z][a-z][a-z][1-9]_[A-Z][a-z].*";
	final static String CATEGORY_SORT_REGEX = "(^$)|(^[0-9])([0-9]?$)";
	final static String SIDE_EFFECT_NAME_REGEX = "^[A-Z][a-zA-Z0-9_]+$";
	final static String THREE_LETTER_SORT_NAME_REGEX = "^[A-Z][a-z][a-z]$";
	final static String CATEGORY_SORT_FIELD_NAME = "Category Sort Id";
	final static String THREE_LETTER_FIELD_NAME = "3 Letter Sort Category";
	final static String SIDE_EFFECT_FIELD_NAME = "SideEffect of [such as vendor, approach, or technology]";
	static final String MAXIMUM_CONTRIBUTION_FIELD_NAME = "Max Contribution to 100% Saturation";
	Page page;
	VerticalLayout layout;
	ScoreTracker scoreTracker = new ScoreTrackerImpl(null);
	Set<String> workRoles = new HashSet<String>();
	Set<String> workRolesAdded = new HashSet<String>();
	String introLabelText = "This is a crude form for helping you quickly write a SMOSLT SideEffects class";
	Label introLabel = new Label(introLabelText);
	Link moreInfoLink = new Link(
			"For more about how this works, what a 'SideEffect' is, etc see http://smoslt.com/generating-sideeffect-classes/ ",
			new ExternalResource(
					"http://smoslt.com/generating-sideeffect-classes/"));
	TextField authorField = new TextField(
			"Author: Proper Name like 'Mary Smith'");
	String threeLetterText;
	String categorySortText;
	String sideEffectName;
	TextField threeLetterSortCategory = new TextField(THREE_LETTER_FIELD_NAME);
	final Label whatCategorySortMeans = new Label(
			"[3 Letter Sort not provided yet]");
	final Label classNameShown = new Label("[no class name to validate yet]");
	TextField categorySortId = new TextField(CATEGORY_SORT_FIELD_NAME);
	TextField sideEffectOf = new TextField(SIDE_EFFECT_FIELD_NAME);
	Button recalculateButton = new Button("Recalulate code window");
	String codeWindowInstructions = "Copy and paste the text in the Code Window below in your IDE into a java class named smoslt.given.sideeffect.out."
			+ threeLetterSortCategory.getValue();
	String firstLine = "/*Copyright 2014 ";
	TextField addedWorkRoles = new TextField(
			"and/or, add work roles (separated by a space)");
	ListSelect workRoleList = new ListSelect(
			"Work Roles - select each that may trigger an adjustment of any score");
	OptionGroup everyOrFirstTask = new OptionGroup(
			"Every and/or first task affects score");
	final Label currentFocus = new Label("Selected: -");
	final TextArea codeWindow = new TextArea("Code Window");
	Label codeWindowAdvice = new Label(codeWindowInstructions);
	Panel classPanel = new Panel("SideEffect Class Setup");
	FormLayout classContent = new FormLayout();
	Panel rolesPanel = new Panel("Work Roles that may affect scoring");
	FormLayout rolesContent = new FormLayout();
	Panel codePanel = new Panel("Generated Code");
	FormLayout codeContent = new FormLayout();
	Map<ScoreKey, ScoreContent> scoreContentMap = new HashMap<ScoreKey, SideEffectGenerate.ScoreContent>();
	Set<String> extraTableColumnHeads = new HashSet<String>();
	Map<ScoreKey, TextField> maxContributionPercentages = new HashMap<ScoreKey, TextField>();
	Map<String, List<SideEffect>> sideEffectMap = new HashMap<String, List<SideEffect>>();

	public SideEffectGenerate(Page page, VerticalLayout layout) {
		this.page = page;
		this.layout = layout;
		initializeWorkRoles();
	}

	private void layoutInitialPanels() {
		layout.addComponent(introLabel);
		layout.addComponent(moreInfoLink);
		classContent.addComponent(authorField);
		classContent.addComponent(threeLetterSortCategory);
		classContent.addComponent(categorySortId);
		classContent.addComponent(sideEffectOf);
		classContent.addComponent(whatCategorySortMeans);
		classContent.addComponent(classNameShown);
		rolesContent.addComponent(workRoleList);
		rolesContent.addComponent(new Label(""));
		rolesContent.addComponent(addedWorkRoles);
		rolesContent.addComponent(new Label(""));
		rolesContent.addComponent(everyOrFirstTask);
		rolesContent.addComponent(currentFocus);
		layout.addComponent(new Label(""));
		layout.addComponent(classPanel);
		layout.addComponent(new Label(""));
		layout.addComponent(rolesPanel);
		layout.addComponent(new Label(""));
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		setValue();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		setValue();
	}

	public void execute() {
		setInitialPanels();
		adjustComponents();
		layoutInitialPanels();
		setScoringPanels();
		layoutFinalPanels();
	}

	private void layoutFinalPanels() {
		codeContent.addComponent(recalculateButton);
		codeContent.addComponent(new Label(""));
		codeContent.addComponent(codeWindowAdvice);
		codeContent.addComponent(codeWindow);
		layout.addComponent(codePanel);
		layout.addComponent(new Label(""));
	}

	private void setScoringPanels() {
		Panel scorePanel;
		FormLayout scorePanelContent;
		Table scoreTable;
		ScoreContent scoreContent;
		TextArea commentTextArea;
		TextField maxContributionTextField = new TextField();
		for (ScoreKey scoreKey : scoreTracker.getHeaders()) {
			if (scoreKey != ScoreKey.FAIL) {
				layout.addComponent(new Label(""));
				scorePanel = new Panel(scoreKey.toString() + " scoring ");
				scorePanelContent = new FormLayout();
				if (scoreKey != ScoreKey.Hours) {
					maxContributionTextField = new TextField(
							MAXIMUM_CONTRIBUTION_FIELD_NAME);
					maxContributionPercentages.put(scoreKey,
							maxContributionTextField);
					scorePanelContent.addComponent(maxContributionTextField);
				}
				scoreTable = getScoreTable(scoreKey);
				scorePanelContent.addComponent(scoreTable);
				commentTextArea = new TextArea("Comments");
				commentTextArea.setSizeFull();
				scorePanelContent.addComponent(commentTextArea);
				scorePanel.setContent(scorePanelContent);
				scorePanel.setSizeFull();
				scorePanelContent.setSizeFull();
				layout.addComponent(scorePanel);
				scoreContent = new ScoreContent(scoreKey, scoreTable,
						commentTextArea, maxContributionTextField);
				scoreContentMap.put(scoreKey, scoreContent);
			}
		}
	}

	private Table getScoreTable(ScoreKey scoreKey) {
		Table scoreTable = new Table(scoreKey.toString() + " scores");
		scoreTable.setImmediate(true);
		scoreTable.setEditable(true);
		scoreTable.setSelectable(true);
		scoreTable.setSizeUndefined();
		scoreTable.addContainerProperty(WORK_COLUMN_NAME, String.class, null);
		scoreTable.setSizeUndefined();
		scoreTable.setPageLength(scoreTable.size());
		scoreTable.setColumnCollapsingAllowed(true);
		return scoreTable;
	}

	private int getScoreIndex(String workRole) {
		int scoreIndex = 0;
		if (workRoles.contains("" + workRole)) {
			for (String item : workRoles) {
				scoreIndex++;
				if (item.equals(workRole)) {
					break;
				}
			}
		} else {
			scoreIndex = workRoles.size();
			workRolesAdded.add(workRole);// no effect if already there
			for (String item : workRolesAdded) {
				scoreIndex++;
				if (item.equals(workRole)) {
					break;
				}
			}
		}
		return scoreIndex;
	}

	private void setInitialPanels() {
		classPanel.setContent(classContent);
		classPanel.setSizeUndefined();
		classContent.setSizeUndefined();
		classContent.setMargin(true);

		rolesPanel.setContent(rolesContent);
		rolesPanel.setSizeUndefined();
		rolesContent.setSizeUndefined();
		rolesContent.setMargin(true);

		codePanel.setContent(codeContent);
		codePanel.setSizeFull();
		codeContent.setSizeFull();
		codeContent.setMargin(true);
	}

	private void adjustComponents() {
		page.setTitle("SMOSLT Side Effect Generator");
		authorField.addValueChangeListener(this);
		authorField.setValue("myDefaultValue");
		authorField.setImmediate(true);
		threeLetterSortCategory.addValueChangeListener(this);
		authorField.setValue("My Name");
		threeLetterSortCategory.setImmediate(true);
		categorySortId.addValueChangeListener(this);
		sideEffectOf.addValueChangeListener(this);
		categorySortId.setImmediate(true);
		workRoleList.setMultiSelect(true);
		workRoleList.setSizeUndefined();
		everyOrFirstTask.setMultiSelect(true);
		everyOrFirstTask
				.addItems(SETUP_TASK_COLUMN_NAME, EACH_TASK_COLUMN_NAME);
		workRoleList.addItems(workRoles);
		workRoleList.addValueChangeListener(new Property.ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				currentFocus.setValue("Selected: " + workRoleList.getValue());
				updateScoringPanels();
			}
		});
		everyOrFirstTask
				.addValueChangeListener(new Property.ValueChangeListener() {
					public void valueChange(ValueChangeEvent event) {
						updateScoringPanels();
					}
				});
		currentFocus.setImmediate(true);
		addRecalculateButtonListener(recalculateButton);
		codeWindow
				.setValue("Code Window - recalculated from your choices above");
		codeWindow.setSizeFull();
	}

	private void updateScoringPanels() {
		for (ScoreKey scoreKey : scoreContentMap.keySet()) {
			Table scoreTable = scoreContentMap.get(scoreKey).getTable();

			reconfigureColumns(everyOrFirstTask.getValue(), scoreTable);
			Item row;
			int roleIndex;
			for (String role : getWorkRoles()) {
				roleIndex = getScoreIndex(role);
				row = scoreTable.addItem(roleIndex);
				if (row == null) {
					row = scoreTable.getItem(roleIndex);
				}
				if (row == null) {
					throw new IllegalAccessError(); // I at least want to know
				}
				if (null == row.getItemProperty(WORK_COLUMN_NAME)) {
					throw new IllegalAccessError(); // I at least want to know
				} else {
					row.getItemProperty(WORK_COLUMN_NAME).setValue(role);
				}
			}

		}
	}

	@SuppressWarnings("unchecked")
	private void exampleOfTableIteration(Table scoreTable) {
		Item row;
		@SuppressWarnings("rawtypes")
		Property property;
		@SuppressWarnings("unused")
		Object value;
		for (Object itemId : scoreTable.getItemIds()) {
			row = scoreTable.getItem(itemId);
			property = row.getItemProperty(EACH_TASK_COLUMN_NAME);
			if (null != scoreTable.getContainerProperty(itemId,
					EACH_TASK_COLUMN_NAME)
					&& null != property
					&& null == property.getValue()) {
				row.getItemProperty(EACH_TASK_COLUMN_NAME).setValue(1f);
			}
			property = row.getItemProperty(SETUP_TASK_COLUMN_NAME);
			if (null != scoreTable.getContainerProperty(itemId,
					SETUP_TASK_COLUMN_NAME)
					&& null != property
					&& null == property.getValue()) {
				row.getItemProperty(SETUP_TASK_COLUMN_NAME).setValue(0f);
			}
		}
	}

	private void reconfigureColumns(Object value, Table scoreTable) {
		Set<String> columnHeads = (Set<String>) value;
		currentFocus.setValue("" + value);
		for (String head : columnHeads) {
			if (head.equals(SETUP_TASK_COLUMN_NAME)) {
				scoreTable.addContainerProperty(head, Float.class, 0f);
			} else if (head.equals(EACH_TASK_COLUMN_NAME)) {
				scoreTable.addContainerProperty(head, Float.class, 1f);
			}
		}
		for (String head : columnHeads) {
			if (head.equals(SETUP_TASK_COLUMN_NAME)) {
				scoreTable.addContainerProperty(SETUP_TASK_CODE_COLUMN_NAME,
						String.class, "");
			}
			if (head.equals(EACH_TASK_COLUMN_NAME)) {
				scoreTable.addContainerProperty(EACH_TASK_CODE_COLUMN_NAME,
						String.class, "");
			}
		}
	}

	private void addRecalculateButtonListener(Button button2) {
		recalculateButton.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				setValue();
			}
		});
	}

	private void setValue() {
		threeLetterText = threeLetterSortCategory.getValue();
		categorySortText = categorySortId.getValue();
		sideEffectName = sideEffectOf.getValue();
		whatCategorySortMeans.setValue(validateClassName());
		classNameShown.setValue(threeLetterText + categorySortText + "_"
				+ sideEffectName);
		StringBuffer sb = new StringBuffer();
		sb.append(firstLine);
		sb.append(authorField.getValue() + "\n");
		sb.append(TemplateString.getImport());
		sb.append(getClassName());
		sb.append(TemplateString.getHead());
		sb.append(getSaturationInit());
		sb.append("    switch (task.getWorkRole()) {\n");
		sb.append(getCaseStatements());
		sb.append("    }\n");
		sb.append(TemplateString.getTail());
		codeWindow.setValue(sb.toString());
	}

	private String getCaseStatements() {
		StringBuffer sb = new StringBuffer();
		if (hasSideEffects()) {
			for (String workRole : getWorkRoles()) {
				System.out.println(workRole + " HERE");
				sb.append(getSideEffect(workRole));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	private boolean hasSideEffects() {
		boolean hasSideEffects = buildSideEffectsMap();
		return hasSideEffects;
	}

	boolean buildSideEffectsMap() {
		boolean hasSideEffects = false;
		sideEffectMap = new HashMap<String, List<SideEffect>>();
		List<SideEffect> sideEffects = new ArrayList<SideEffectGenerate.SideEffect>();
		for (ScoreKey scoreKey : scoreContentMap.keySet()) {
			Table scoreTable = scoreContentMap.get(scoreKey).getTable();
			Item row;
			String workRole;
			String each;
			String eachCode;
			String first;
			String firstCode;
			for (Object itemId : scoreTable.getItemIds()) {
				row = scoreTable.getItem(itemId);
				workRole = ""
						+ row.getItemProperty(WORK_COLUMN_NAME).getValue();
				each = ""
						+ row.getItemProperty(EACH_TASK_COLUMN_NAME).getValue();
				eachCode = ""
						+ row.getItemProperty(EACH_TASK_CODE_COLUMN_NAME)
								.getValue();
				first = ""
						+ row.getItemProperty(SETUP_TASK_COLUMN_NAME)
								.getValue();
				firstCode = ""
						+ row.getItemProperty(SETUP_TASK_CODE_COLUMN_NAME)
								.getValue();
				if ((!each.equals("null") && !each.equals("1.0"))
						|| (!first.equals("null") && !first.equals("0.0"))
						|| (!eachCode.equals("null") && !eachCode.isEmpty())
						|| (!firstCode.equals("null") && !firstCode.isEmpty())) {
//					System.out.println("EACH: '" + each + "' EACHCODE: '"
//							+ eachCode + "' FIRST: '" + first
//							+ "' FIRSTCODE: '" + firstCode + "' NAME: '"
//							+ workRole + "' SCOREKEY: " + scoreKey);
					SideEffect sideEffect = new SideEffect();
					sideEffect.scoreKey = scoreKey;
					sideEffect.workRole = workRole;
					sideEffect.each = each;
					sideEffect.eachCode = eachCode;
					sideEffect.first = first;
					sideEffect.firstCode = firstCode;
					sideEffects.add(sideEffect);
				}
				for (SideEffect sideEffect : sideEffects) {
					List<SideEffect> workRoleSideEffects = sideEffectMap
							.get(sideEffect.workRole);
					if (null == workRoleSideEffects) {
						workRoleSideEffects = new ArrayList<SideEffectGenerate.SideEffect>();
						sideEffectMap.put(sideEffect.workRole,
								workRoleSideEffects);
					}
					workRoleSideEffects.add(sideEffect);
//					System.out.println("\tEACH: '" + each + "' EACHCODE: '"
//							+ eachCode + "' FIRST: '" + first
//							+ "' FIRSTCODE: '" + firstCode + "' NAME: '"
//							+ workRole + "' SCOREKEY: " + scoreKey);
					hasSideEffects = true;
				}
			}
		}
		return hasSideEffects;
	}

	private Object getSideEffect(String workRole) {
		StringBuffer sb = new StringBuffer();
		if (hasSideEffects(workRole)) {
			sb.append("    case \"" + workRole + "\":\n");
			sb.append("        if (isFirstTaskWith(task.getWorkRole())) { // for \""
					+ workRole + "\" only\n");
			sb.append(getFirstTaskCode(workRole));
			sb.append("        } else { // for every \"" + workRole
					+ "\" task\n");
			sb.append("            task.modifyDuration(" + 9999
					+ ");// modify number of hours by...\n");
			sb.append("        }\n");
			sb.append("        break;// do not remove break!\n");
		}
		return sb.toString();
	}

//	sb.append("modifyDuration("+sideEffect.first+");/n");
	private String getFirstTaskCode(String workRole) {
		StringBuffer sb = new StringBuffer();
		for(SideEffect sideEffect:sideEffectMap.get(workRole)){
			if(sideEffect.first!=null&&!sideEffect.first.equals("0.0")){
				if(sideEffect.scoreKey==ScoreKey.Hours){
					sb.append("addDuration("+sideEffect.first+");\n");
				}else{
					sb.append("incrementSaturationLimit(ScoreKey."+sideEffect.scoreKey+","+");\n");
				}
			}
			if(sideEffect.firstCode!=null&&!sideEffect.firstCode.isEmpty()){
				sb.append(sideEffect.firstCode+"\n");
			}
		}
		return sb.toString();
	}

	private boolean hasSideEffects(String workRole) {
		boolean hasSideEffects = false;
		List<SideEffect> sideEffects = sideEffectMap.get(workRole);
		if (sideEffects != null && sideEffects.size() > 0) {
			hasSideEffects = true;
		}
		return hasSideEffects;
	}

	private Object getSaturationInit() {
		StringBuffer sb = new StringBuffer();
		ScoreContent scoreContent;
		String maxContribution;
		int maxContributionValue = 0;
		for (ScoreKey scoreKey : scoreTracker.getHeaders()) {
			if (ScoreKey.FAIL != scoreKey && ScoreKey.Hours != scoreKey) {
				scoreContent = scoreContentMap.get(scoreKey);
				if (null != scoreContent) {
					maxContribution = scoreContent
							.getMaxContributionTextField().getValue();
					if (null != maxContribution && !maxContribution.isEmpty()) {
						try {
							maxContributionValue = Integer
									.valueOf(maxContribution.trim());
						} catch (NumberFormatException nfe) {
							// expected
						}
						if (maxContributionValue != 0) {
							sb.append("        initializeSaturationLimit(ScoreKey."
									+ scoreKey
									+ ", "
									+ maxContributionValue
									+ ");\n");
						}
					}
				}
			}
		}
		return sb.toString();
	}

	private String validateClassName() {
		StringBuffer sb = new StringBuffer();
		sb.append(validateThreeLetterText());
		sb.append(validateCategoryCodeText());
		sb.append(validateSideEffectName());
		return sb.toString();
	}

	private String validateSideEffectName() {
		String returnMessage = "OK: " + SIDE_EFFECT_FIELD_NAME;
		if (!sideEffectName.matches(SIDE_EFFECT_NAME_REGEX)) {
			returnMessage = "NOT COMPLIANT: " + SIDE_EFFECT_FIELD_NAME;
		}
		return returnMessage;
	}

	private String validateCategoryCodeText() {
		String returnMessage = "CategoryCode of '' assumes that this is a one of, and no other competing alternatives in this category";
		if (!categorySortText.matches(CATEGORY_SORT_REGEX)) {
			returnMessage = "NOT COMPLIANT: " + CATEGORY_SORT_FIELD_NAME;
		}
		return returnMessage;
	}

	private Object validateThreeLetterText() {
		String returnMessage = "OK: " + THREE_LETTER_FIELD_NAME;
		if (!threeLetterText.matches(THREE_LETTER_SORT_NAME_REGEX)) {
			returnMessage = "NOT COMPLIANT: " + THREE_LETTER_FIELD_NAME;
		}
		return returnMessage;
	}

	private Object getClassName() {
		String categorySort = categorySortId.getValue();
		try {
			int categeorySortValue = Integer.valueOf(categorySort);
		} catch (NumberFormatException nfe) {
			categorySort = "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append(threeLetterSortCategory.getValue());
		sb.append("" + categorySort + "_");
		sb.append(StringUtils_.upStart(sideEffectOf.getValue()));
		return sb.toString();
	}

	private List<String> getWorkRoles() {
		List<String> workRoles = new ArrayList<String>();
		if (null != workRoleList && null != workRoleList.getValue()) {
			workRoles.addAll((Set<String>) workRoleList.getValue());
			String addedRolesSpaceDelimited = addedWorkRoles.getValue();
			if (addedRolesSpaceDelimited != null
					&& addedRolesSpaceDelimited.trim().length() > 0) {
				String[] values = addedRolesSpaceDelimited.split(" ");
				for (int i = 0; i < values.length; i++) {
					workRoles.add(values[i]);
				}
			}
		}
		return workRoles;
	}

	private void initializeWorkRoles() {
		workRoles.add("DevImpl");
		workRoles.add("DevDesign");
		workRoles.add("UIDesign");
		workRoles.add("QA");
		workRoles.add("BusAnlyst");
		workRoles.add("PrjMngmt");
		workRoles.add("Ops");
		workRoles.add("DatScntst");
	}

	class ScoreContent {
		private ScoreKey scoreKey;
		private Table table;
		private TextArea commentTextArea;
		private TextField maxContributionTextField;

		ScoreContent(ScoreKey scoreKey, Table table, TextArea commentTextArea,
				TextField maxContributionTextField) {
			this.scoreKey = scoreKey;
			this.table = table;
			this.commentTextArea = commentTextArea;
			this.maxContributionTextField = maxContributionTextField;
		}

		public ScoreKey getScoreKey() {
			return scoreKey;
		}

		public Table getTable() {
			return table;
		}

		public TextArea getCommentTextArea() {
			return commentTextArea;
		}

		public TextField getMaxContributionTextField() {
			return maxContributionTextField;
		}

	}

	class ScoreRowValues {
		public BigDecimal getTask() {
			return task;
		}

		private String id;
		private BigDecimal setup;
		private BigDecimal task;

		ScoreRowValues(String id, BigDecimal setup, BigDecimal task) {
			this.id = id;
			this.setup = setup;
			this.task = task;
		}

		ScoreRowValues(String id, BigDecimal task) {
			this.id = id;
			this.task = task;
		}

		ScoreRowValues(BigDecimal setup, String id) {
			this.id = id;
			this.setup = setup;
		}

		public String getId() {
			return id;
		}

		public BigDecimal getSetup() {
			return setup;
		}

	}

	class SideEffect {
		ScoreKey scoreKey;
		String workRole;
		String first;
		String firstCode;
		String each;
		String eachCode;
	}

}
