package smoslt;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
//import java.util.UUID;



import smoslt.domain.Models;
import smoslt.domain.ProjectProfile;
import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectFactorType;
import smoslt.generator.ProjectProfileFactory;
import smoslt.generator.ScheduleFromProfile;
import smoslt.util.ConfigProperties;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class ProjectGenerate {
	/*
	 * Business API Mobile App Content Delivery Batch Transform Big Data ETL &
	 * Analysis Desktop Data Tool Corporate BI Rollout Streaming Data Input
	 * Service ETL and Analysis
	 */
	Page page;
	VerticalLayout layout;
	ProjectProfile projectProfile;
	Map<Component, ProjectFactor> componentMap = new HashMap<Component, ProjectFactor>();
	Set<String> categories = new TreeSet<String>();
	int modelCount = 25;
	String domainModel = "Suppliers\n" + "SupplierType\n"
			+ "MaintenanceContract\n" + "Staff\n" + "Asset\n" + "AssetPart\n"
			+ "FaultLog\n" + "Part\n" + "FaultLogPart\n" + "PartFault\n"
			+ "SkillType\n" + "SkillRequired\n" + "Skill\n" + "EngineerVisit\n"
			+ "RefFaultStatus\n" + "EngineerSkill\n" + "StaffType\n"
			+ "AssetVendor\n" + "ShippingType\n" + "ShippingVendor\n"
			+ "Inventory\n" + "EngineerVisit\n" + "MaintenanceEngineer\n"
			+ "ProductHierarchy\n" + "ProductSupplier";
	String domainType = "Manufacturing";
	File sourceFile = null;
	final static String WEB_CLASS_DIR = "/WEB-INF/classes/";
	private static final String WEB_GENERATED_DIR = "/WEB-INF/generated/";

	public ProjectGenerate(Page page, VerticalLayout layout) {
		this.page = page;
		this.layout = layout;
		initializeSourceFile();
	}

	public void execute() {
		projectProfile = ProjectProfileFactory.get(sourceFile);
		if (projectProfile != null) {
			processCategories();
			page.setTitle("SMOSLT Sample Project Generator");
			String introLabelText = "This is a crude form for helping you write a SMOSLT Sample Projects";
			Label introLabel = new Label(introLabelText);
			layout.addComponent(introLabel);
			Link moreInfoLink = new Link(
					"For more information about what a 'SideEffect' class is see http://smostl.com/generatingSideEffects",
					new ExternalResource(
							"http://smostl.com/generatingSideEffects"));
			layout.addComponent(moreInfoLink);
			createComponents();
			adjustComponents();
			Button button = new Button("Generate New ProjectLibre File");
			button.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					layout.addComponent(new Label("Thank you for clicking"));
					setValues();
					generateNewProject();
				}
			});
			layout.addComponent(button);
		}
	}

	private void initializeSourceFile() {
		String basepath = VaadinService.getCurrent().getBaseDirectory()
				.getAbsolutePath();
		FileResource resource = new FileResource(new File(basepath
				+ WEB_CLASS_DIR
				+ ScheduleFromProfile.PROJECT_PROFILE_JSON_FILE_NAME));
		sourceFile = resource.getSourceFile();
	}

	private void adjustComponents() {
		addDomainTypeListener();
		addDomainCountListener();
		adjustSoftwareProcesses();
		setDomainTables();
	}

	private void setDomainTables() {
		final OptionGroup optionGroup = (OptionGroup) getComponent("domainSampleSet");
		domainType = "" + optionGroup.getValue();
		resetDomainModel();
	}

	private void adjustSoftwareProcesses() {
		final OptionGroup optionGroup = (OptionGroup) getComponent("softwareProcesses");
		optionGroup.setMultiSelect(true);
		// Set<String> options = (Set<String>);
		for (Object option : (Set<?>) optionGroup.getValue()) {
			System.out
					.println("HERE I WAS INTENDING TO DO SOMETHING NOT SURE WHAT "
							+ option);
		}
	}

	private void addDomainCountListener() {
		final Slider slider = (Slider) getComponent("domainModelCount");
		slider.addValueChangeListener(new Property.ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				modelCount = slider.getValue().intValue();
				resetDomainModel();
			}
		});
	}

	private void resetDomainModel() {
		switch (domainType) {
		case Models.MODEL_CUSTOM:
			setDomains(null);
			break;
		case Models.MODEL_EDUCATION:
			setDomains(Models.EDUCATION_MODELS);
			break;
		case Models.MODEL_HR:
			setDomains(Models.HR_MODELS);
			break;
		case Models.MODEL_INSURANCE:
			setDomains(Models.INSURANCE_MODELS);
			break;
		case Models.MODEL_INVENTORY:
			setDomains(Models.INVENTORY_MODELS);
			break;
		case Models.MODEL_LOGISTICS:
			setDomains(Models.LOGISTICS_MODELS);
			break;
		case Models.MODEL_MANUFACTURING:
			setDomains(Models.MANUFACTURING_MODELS);
			break;
		case Models.MODEL_MEDICAL:
			setDomains(Models.MEDICAL_MODELS);
			break;
		case Models.MODEL_REAL_ESTATE:
			setDomains(Models.REAL_ESTATE_MODELS);
			break;
		case Models.MODEL_RETAIL:
			setDomains(Models.RETAIL_MODELS);
			break;
		case Models.MODEL_SOCIAL_MEDIA:
			setDomains(Models.SOCIAL_MEDIA_MODELS);
			break;
		}
	}

	private void setDomains(String domainModelInput) {
		final TextArea textArea = (TextArea) getComponent("domainTableNames");
		if (domainModelInput == null) {
			domainModelInput = textArea.getValue();
		}
		limitToModelCount(domainModelInput);
		textArea.setValue(domainModel);
		textArea.setSizeFull();
	}

	private void limitToModelCount(String domainModelInput) {
		StringTokenizer stk = new StringTokenizer(domainModelInput, "\n");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < modelCount; i++) {
			if (stk.hasMoreTokens()) {
				sb.append(stk.nextToken() + "\n");
			}
		}
		domainModel = sb.toString();
		domainModel = sb.substring(0, domainModel.length() - 1);
	}

	private Component getComponent(String string) {
		for (Component component : componentMap.keySet()) {
			if (component.getId().equals(string)) {
				return component;
			}
		}
		throw new IllegalArgumentException("Request for component with id of '"
				+ string + "' which did not exist");
	}

	private void addDomainTypeListener() {
		final OptionGroup optionGroup = (OptionGroup) getComponent("domainSampleSet");
		optionGroup.addValueChangeListener(new Property.ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				domainType = "" + optionGroup.getValue();
				resetDomainModel();
			}
		});
	}

	void generateNewProject() {
		String basepath = VaadinService.getCurrent().getBaseDirectory()
				.getAbsolutePath();
		File file = new File(basepath + WEB_GENERATED_DIR
				+ConfigProperties.get().getProperty(ConfigProperties.PROJECT_NAME) + ".pod");
		FileResource fileResource = new FileResource(file);
		new ScheduleFromProfile(projectProfile).execute(file);
		Link link = new Link("Link to the generated file", fileResource);
		layout.addComponent(link);
	}

	private void processCategories() {
		for (ProjectFactor projectFactor : projectProfile.getProjectFactors()
				.values()) {
			categories.add(projectFactor.getCategory());
		}
	}

	private void setValues() {
		for (Component component : componentMap.keySet()) {
			ProjectFactor projectFactor = componentMap.get(component);
			setValue(component, projectFactor);
		}
	}

	private void setValue(Component component, ProjectFactor projectFactor) {
		if (projectFactor.deduceProjectFactorType() == ProjectFactorType.Slider
				&& component instanceof Slider) {
			projectFactor.setValue("" + ((Slider) component).getValue());
		} else if (projectFactor.deduceProjectFactorType() == ProjectFactorType.TextField
				&& component instanceof TextField) {
			projectFactor.setValue("" + ((TextField) component).getValue());
		} else if (projectFactor.deduceProjectFactorType() == ProjectFactorType.TextArea
				&& component instanceof TextArea) {
			projectFactor.setText("" + ((TextArea) component).getValue());
		} else if (projectFactor.deduceProjectFactorType() == ProjectFactorType.MultipleChoice
				&& component instanceof OptionGroup) {
			setOptionValue(component, projectFactor);
		} else {
			throw new IllegalArgumentException();
		}
	}

	private void setOptionValue(Component component, ProjectFactor projectFactor) {
		OptionGroup optionGroup = (OptionGroup) component;
		String value = optionGroup.getValue().toString();
		if (value.startsWith("[")) {
			projectFactor.setValue(value);
		} else {
			projectFactor.setChoice(Integer.valueOf(value));
		}
	}

	void createComponents() {
		for (String category : categories) {
			FormLayout contentLayout = getPanel(category);
			for (ProjectFactor projectFactor : projectProfile
					.getProjectFactors().values()) {
				if (projectFactor.getCategory().equals(category)) {
					if (ProjectFactorType.MultipleChoice == projectFactor
							.deduceProjectFactorType()) {
						addOptionGroup(projectFactor, contentLayout);
					} else if (ProjectFactorType.Slider == projectFactor
							.deduceProjectFactorType()) {
						addSlider(projectFactor, contentLayout);
					} else if (ProjectFactorType.TextArea == projectFactor
							.deduceProjectFactorType()) {
						addTextArea(projectFactor, contentLayout);
					} else if (ProjectFactorType.TextField == projectFactor
							.deduceProjectFactorType()) {
						addTextField(projectFactor, contentLayout);
					}
				}
			}
		}
	}

	private FormLayout getPanel(String category) {
		Panel panel = new Panel(getCategoryDefinition(category));
		FormLayout contentPanel = new FormLayout();
		Label space = new Label(" ");
		layout.addComponent(space);
		layout.addComponent(panel);
		panel.setContent(contentPanel);
		panel.setSizeUndefined();
		contentPanel.setSizeUndefined();
		contentPanel.setMargin(true);
		return contentPanel;
	}

	private String getCategoryDefinition(String category) {
		String categoryDefinition = category;
		if (projectProfile.getCategories().containsKey(category)) {
			categoryDefinition = projectProfile.getCategories().get(category);
		}
		return categoryDefinition;
	}

	private void addSlider(ProjectFactor projectFactor, FormLayout contentPanel) {
		Slider slider = new Slider();
		addComponent(slider, projectFactor, contentPanel);
		slider.setMax(projectFactor.getTopRange());
		slider.setMin(projectFactor.getBottomRange());
		slider.setValue((double) projectFactor.getChoice());
		slider.setImmediate(true);
		slider.setDescription(projectFactor.getDescription());
	}

	private void addTextField(ProjectFactor projectFactor,
			FormLayout contentPanel) {
		TextField textField = new TextField();
		addComponent(textField, projectFactor, contentPanel);
		textField.setValue(projectFactor.getValue());
		textField.setImmediate(true);
		textField.setDescription(projectFactor.getDescription());
	}

	private void addTextArea(ProjectFactor projectFactor,
			FormLayout contentPanel) {
		TextArea textArea = new TextArea();
		addComponent(textArea, projectFactor, contentPanel);
		textArea.setValue(projectFactor.getText());
		textArea.setImmediate(true);
		textArea.setDescription(projectFactor.getDescription());

	}

	void addOptionGroup(ProjectFactor projectFactor, FormLayout contentPanel) {
		OptionGroup optionGroup = new OptionGroup();
		addComponent(optionGroup, projectFactor, contentPanel);
		Object itemId = null;
		Object itemSelected = null;
		int i = 0;
		for (String option : projectFactor.getChoices()) {
			itemId = optionGroup.addItem();
			optionGroup.setItemCaption(itemId, option);
			if (i == projectFactor.getChoice()) {
				itemSelected = itemId;
			}
			i++;
		}
		optionGroup.setValue(itemSelected);
		optionGroup.setImmediate(true);
		optionGroup.setDescription(projectFactor.getDescription());
	}

	private void addComponent(Component component, ProjectFactor projectFactor,
			FormLayout contentPanel) {
		component.setId(projectFactor.getId());
		componentMap.put(component, projectFactor);
		// optionGroup.select(options[selectedIndex]); hmm not sure
		String name = component.getCaption();
		Label space = new Label(" ");
		Label label = new Label(projectFactor.getName());
		label.setWidth("100%");
		contentPanel.addComponent(space);
		contentPanel.addComponent(label);
		contentPanel.addComponent(component);
	}

}
