package smoslt;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class UiExperiments {
	Page page;
	VerticalLayout layout;
	ComboBox comboBox = new ComboBox("Wah");

	public UiExperiments(Page page, VerticalLayout layout) {
		this.page = page;
		this.layout = layout;
	}

	public void execute() {
		page.setTitle("SMOSLT Sample Project Generator");
		String introLabelText = "This is a crude form for helping you write a SMOSLT Sample Projects";
		Label introLabel = new Label(introLabelText);
		layout.addComponent(introLabel);

		Link moreInfoLink = new Link(
				"For more information about what a 'SideEffect' class is see http://smostl.com/generatingSideEffects",
				new ExternalResource("http://smostl.com/generatingSideEffects"));
		layout.addComponent(moreInfoLink);

		Button button = new Button("Click Me");
		button.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				layout.addComponent(new Label("Thank you for clicking"));
			}
		});
		layout.addComponent(button);
		TextArea codeWindow = new TextArea("Code Window");
		codeWindow
				.setValue("this is generated automagically from your choices above");
		layout.addComponent(codeWindow);
		comboBox.addItems("one", "two", "three", "four", "five", "six", "seven");
		comboBox.setValue("hisDefaultValue");
//		comboBox.addValueChangeListener(this);
		comboBox.setImmediate(true);
		comboBox.setNewItemsAllowed(true);
		layout.addComponent(comboBox);
	}

}
