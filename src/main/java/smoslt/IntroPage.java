package smoslt;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

public class IntroPage {
	Page page;
	VerticalLayout layout;

	public IntroPage(Page page, VerticalLayout layout) {
		this.page = page;
		this.layout = layout;
	}

	public void execute() {
		layout.addComponent(new Label("Welcome to SMOSLT Web Generator"));
		page.setTitle("SMOSLT Web Generator Menu of Options");
		Link generateProjectLink = new Link(
				"Generate Sample Projects",
				new ExternalResource("projectGenerator"));
		layout.addComponent(generateProjectLink);
		Link generateSideEffectLink = new Link(
				"Generate SideEffect Classes",
				new ExternalResource("sideEffectGenerator"));
		layout.addComponent(generateSideEffectLink);
		Link uiExeperimentsLink = new Link(
				"UI experiments",
				new ExternalResource("uiExperiments"));
//		layout.addComponent(uiExeperimentsLink);
		
	}

}
