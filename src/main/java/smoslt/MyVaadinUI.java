package smoslt;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "smoslt.AppWidgetSet")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		final VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		setContent(layout);
//		getPage().setTitle("MyTitle");
		String page = request.getPathInfo();
		if (page == null || page.isEmpty() || "/".equals(page)) {
			new IntroPage(getPage(), layout).execute();
		} else if ("/projectGenerator".equals(page)) {
			new ProjectGenerate(getPage(), layout).execute();
		} else if ("/sideEffectGenerator".equals(page)) {
			new SideEffectGenerate(getPage(), layout).execute();
		} else if ("/uiExperiments".equals(page)) {
			new UiExperiments(getPage(), layout).execute();
		}
	}

}
