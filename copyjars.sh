cp ../smoslt.mprtxprt/deps/openproj-contrib ~/dev/apache-tomcat-8.0.15/webapps/smoslt/WEB-INF/lib/openproj-contrib.jar
cp ../smoslt.mprtxprt/deps/openproj-reports ~/dev/apache-tomcat-8.0.15/webapps/smoslt/WEB-INF/lib/openproj-reports.jar
cp ../smoslt.mprtxprt/deps/openproj-script ~/dev/apache-tomcat-8.0.15/webapps/smoslt/WEB-INF/lib/openproj-script.jar
cp ../smoslt.mprtxprt/deps/projectlibre ~/dev/apache-tomcat-8.0.15/webapps/smoslt/WEB-INF/lib/projectlibre.jar
mkdir ~/dev/apache-tomcat-8.0.15/webapps/smoslt/WEB-INF/generated
~/dev/apache-tomcat-8.0.15/bin/shutdown.sh
~/dev/apache-tomcat-8.0.15/bin/startup.sh